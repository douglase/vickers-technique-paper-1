#!/bin/sh
#
 echo " Starting the AURIC batch run."
#
AURIC_BIN_DIR="${AURIC_ROOT}"/bin/`uname`
#
$AURIC_BIN_DIR/atmos
$AURIC_BIN_DIR/ionos
$AURIC_BIN_DIR/solar
$AURIC_BIN_DIR/colden
$AURIC_BIN_DIR/pesource
$AURIC_BIN_DIR/peflux
$AURIC_BIN_DIR/e_impact
$AURIC_BIN_DIR/daychem
$AURIC_BIN_DIR/mergever
$AURIC_BIN_DIR/losden
$AURIC_BIN_DIR/radtrans
$AURIC_BIN_DIR/losint
$AURIC_BIN_DIR/mergeint
#
 echo " AURIC batch run is completed."
#
