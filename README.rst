.. role:: raw-latex(raw)
            :format: latex html

        .. raw:: html

           <script type="text/javascript" src="http://localhost/mathjax/MathJax.js?config=TeX-AMS_HTML"></script>

Matrix-Based Resonant Scattering Paper Outline
====

Introduction
---------------

- Radiative transfer in the atmosphere is typically solved via the Feautrier Method which trades a first order differential equation for two second-order equations.

    - These equations are solved iteratively, which takes time.

- When solving inverse problems, it is necessary to solve the RT problem many times over to compare with observations
- One such inverse problem is the retrieval of ionospheric (actually O+) parameters from OII 83.4 nm EUV dayglow profiles.
- This can be done, but it is very slow and/or imprecise using a feautrier method. (there is a tradeoff between speed and precision of course)

Model Description
------------------------

- Scattering in an optically thick medium is a Markov process

    - Photons are probabilistically redistributed according to a transition matrix (single scattering matrix)
    - The mapping between source mission and observed emission is a geometric sum in the scattering matrix

- Under certain mathematical conditions, this means the mapping from photoproduction to scattered emission is very easy to calculate

    1. Eigenvalues of transition matrix smaller than one -> convergence of sum
    2. Eigenvalues of (I-M) nonzero -> sum equals inverse of (I-M) (multiple scattering matrix)

- Geometric sum arises because photons emitted from a point in the medium are a sum of those produced there, produced elsewhere and scattered, produced  and scattered twice, thrice, and so on
- Maybe talk about how the viewing matrix mucks with things, since it is inevitable if we want to use this for real observations.

Derivation/Application to  OII 83.4 nm 
-------------------------------------------------

- Several assumptions are made for simplicity

    1. Isotropic Scattering

        - This is fine as long as medium is not too thick (few scatterings per photon)

    2. Complete frequency redistribution according to voigt profile
    3. Altitude bins must be small enough that scattering into other bins occurs more than scattering within the same bin.

- Current cross section measurements are fairly uncertain, so there are many assumptions buried in them.
- MSIS is used for neutral densities and temperatures
- AURIC is used for the source profile

    - Optically thin emission like OII 61.7 nm is also an option here

Comparison with Feautrier Method 
----------------------------------------------

- Vickers' method is ~6 times faster than AURIC in my test.
- 6 Times faster means 6 times better resolution of parameters when solving the inverse problem
- Matrix method will fail under certain condtions:

    1. The medium is thick enough for many scatterings to occur

        - Eigenvalues of scattering matrix will be large -> small eigenvalues in (I-M) -> large uncertainties after inverting matrix

    2. Population inversion becomes significant (also when medium is thick)
    3. Absorption is very high compared to scattering (eigenvalues of scattering matrix will be uncertain, I think)

Conclusion 
---------------

- This method should work well given sufficiently accurate EUV data
- Future missions such as LITES can benefit from this